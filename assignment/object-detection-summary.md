# Object Detection

<div align="center">
<img src='../Images/Object-Detection/detection.png' >
<p>Object Detection with an accuracy from a frame of picture</p>
</div>

Object detection is a computer vision technique that allows us to identify and locate objects in an image or video. With this kind of identification and localization, object detection can be used to count objects in a scene and determine and track their precise locations, all while accurately labeling them.
It’s now possible to recognize images or even find objects inside an image with a standard GPU. The objection detection algorithm named **faster R-CNN** 

## Alexnet 
Alexnet is composed of 5 convolutional layers (C1 to C5 on schema) followed by two fully connected (FC6 and FC7), and a final softmax output layer (FC8). It was initially trained to recognize 1000 different objects.

<div align="center">
<img src='../Images/Object-Detection/alexnet_arch.png' >
<p>Alexnet Architecture</p>
</div>

The intuition behind this net is that each convolutional layer learns a more detailed representation of the images (feature map) than the previous one. For example the first layer is able to recognize very simple forms or colors, and the last one more complex forms such as full faces for instance.

The convolutional layers are representing the image in a much better way for the classification. After the convolutions, each image is represented as a vector of 4096 features (whereas they were initially vectors of 227*227*3 = 154 587 features).

<div align="center">
<img src='../Images/Object-Detection/layers.jpg' >
<p>Layers and their abstraction power in a deep net</p>
</div> 

The two fully connected and softmax layers are similar to a multi layer perception and could actually be replaced by other kinds of classifiers such as Random Forests or SVMs. However they are really important for the training phase of the neural net.

## Understanding Region-Based Convolutional Neural Networks (R-CNN)

R-CNN was the first algorithm to apply deep learning to the object detection task. It beats the previous ones by more than 30% on the VOC2012 (Visual Object Classes Challenge) and was therefore a huge improvement in the fields of Object detection.
As previously mentioned, Object Detection presents two difficulties : finding objects and classifying them. That’s the point of R-CNN: dividing the hard task of object detection in two easier ones:

1. Objects Proposal (finding objects)
2. Region Classification (understanding them)

<div align="center">
<img src='../Images/Object-Detection/r-cnn.png' >
<p>R-CNN Classification of Region Proposals</p>
</div> 


R-CNN is actually independent of the Object Proposal algorithm and can use any of these methods.
R-CNN takes as input the regions (or objects or boxes) proposals. Most Region Proposal algorithms output a great number of regions (around 2000 for a standard image) and R-CNN objective is to find which regions are significant and the objects they represent.


<div align="center">
<img src='../Images/Object-Detection/r-cnn-layers.png'>
<p>R-CNN in a nutshell that how it helps us to detect objects</p>
</div> 


### Issues with R-CNN Model

So far, we’ve seen how RCNN can be helpful for object detection. But this technique comes with its own limitations. Training an RCNN model is expensive and slow thanks to the below steps:

1. Extracting 2,000 regions for each image based on selective search
2. Extracting features using CNN for every image region. Suppose we have N images, then the number of CNN features will be N*2,000
3. The entire process of object detection using RCNN has three models:

- CNN for feature extraction.

- Linear SVM classifier for identifying objects.

- Regression model for tightening the bounding boxes.

All these processes combine to make RCNN very slow. It takes around 40-50 seconds to make predictions for each new image, which essentially makes the model cumbersome and practically impossible to build when faced with a gigantic dataset.


## Fast R-CNN

The main goal of Fast-RCNN is to improve these two algorithms by extracting the features and making an end-to-end classification with a single Algorithm. It is easier to train, faster at test time (the classification phase is almost real-time) and it is more accurate. However, this algorithm is still independent of the box proposal algorithm. So regions are still proposed apart, by another algorithm.

<div align="center">
<img src='../Images/Object-Detection/fast-r-cnn.png'>
<p>Fast R-CNN</p>
</div> 

The principle here is similar to SPP-Net: the convolution map will only be computed once for the whole image and then each region proposal will be projected on it before going through a specific pooling layer and fully connected ones.












