# Pose Estimation

<div align="center">
<img src='../Images/Pose-Estimation/pose_face_hands.gif'>
<p>Two guys trying to perform various pose</p>
</div> 

Pose estimation is a key component in enabling machines to have an understanding of people in
images and videos. **OpenPose**represents the first real-time multi-person system to jointly detect human body, hand, facial, and foot keypoints (in total 135 keypoints) on single images. It represents the orientation of a person in a graphical format. Essentially, it is a set of coordinates that can be connected to describe the pose of the person. Each co-ordinate in the skeleton is known as a part (or a joint, or a keypoint). A valid connection between two parts is known as a pair (or a limb). Note that, not all part combinations give rise to valid pairs.

<div align="center">
<img src='../Images/Pose-Estimation/single_pose.png'>
<p>Left: COCO keypoint format for human pose skeletons. Right: Rendered human pose skeletons.</p>
</div> 

Knowing the orientation of a person opens avenues for several real-life applications, some of which are discussed towards the end of this blog. Several approaches to Human Pose Estimation were introduced over the years. The earliest (and slowest) methods typically estimating the pose of a single person in an image which only had one person to begin with. These methods often identify the individual parts first, followed by forming connections between them to create the pose.

## Multi-Person Pose Estimation


Multi-Person pose estimation is more difficult than the single person case as the location and the number of people in an image are unknown. Typically, we can tackle the above issue using one of two approaches:

-The simple approach is to incorporate a person detector first, followed by estimating the parts and then calculating the pose for each person. This method is known as the top-down approach.

-Another approach is to detect all parts in the image (i.e. parts of every person), followed by associating/grouping parts belonging to distinct persons. This method is known as the bottom-up approach.

<div align="center">
<img src='../Images/Pose-Estimation/pose.png'>
<p>Multi-Person Pose Estimation</p>
</div> 

## OpenPose Approach for Pose Estimation

OpenPose is one of the most popular bottom-up approaches for multi-person human pose estimation, partly because of their well documented GitHub implementation.
As with many bottom-up approaches, OpenPose first detects parts (keypoints) belonging to every person in the image, followed by assigning parts to distinct individuals. Shown below is the architecture of the OpenPose model.


<div align="center">
<img src='../Images/Pose-Estimation/architecture.png'>
<p>Flowchart of the OpenPose Architecture</p>
</div> 

The OpenPose network first extracts features from an image using the first few layers (VGG-19 in the above flowchart). The features are then fed into two parallel branches of convolutional layers. The first branch predicts a set of 18 confidence maps, with each map representing a particular part of the human pose skeleton. The second branch predicts a set of 38 Part Affinity Fields (PAFs) which represents the degree of association between parts.

<div align="center">
<img src='../Images/Pose-Estimation/overall-pipeline.png'>
<p>Steps involved in human pose estimation using OpenPose</p>
</div> 

Successive stages are used to refine the predictions made by each branch. Using the part confidence maps, bipartite graphs are formed between pairs of parts (as shown in the above image). Using the PAF values, weaker links in the bipartite graphs are pruned. Through the above steps, human pose skeletons can be estimated and assigned to every person in the image.


There are some more models for Pose Estimation which are:

- DeepCut

- RMPE (AlphaPose)

- Mask R-CNN

- Awesome Human Pose Estimation

## Applications of Pose Estimation

- Activity Recognition

Tracking the variations in the pose of a person over a period of time can also be used for activity, gesture and gait recognition. There are several use cases for the same, including:

1. It detect if a person has fallen down or is sick.
2. It can autonomously teach proper work out regimes, sport techniques and dance activities.

<div align="center">
<img src='../Images/Pose-Estimation/activity.png'>
<p>Tracking the gait of the person is useful for security and surveillance purposes</p>
</div> 

- Motion Capture and Augmented Reality

An interesting application of human pose estimation is for CGI applications. Graphics, styles, fancy enhancements, equipment and artwork can be superimposed on the person if their human pose can be estimated. By tracking the variations of this human pose, the rendered graphics can “naturally fit” the person as they move.

<div align="center">
<img src='../Images/Pose-Estimation/ar.jpeg'>
<p>CGI Rendering</p>
</div> 

-  Motion Tracking for Consoles

An interesting application of pose estimation is for tracking the motion of human subjects for interactive gaming. Popularly, Kinect used 3D pose estimation (using IR sensor data) to track the motion of the human players and to use it to render the actions of the virtual characters.

<div align="center">
<img src='../Images/Pose-Estimation/motion_tracking.png'>
<p>The Kinect sensor in action</p>
</div> 