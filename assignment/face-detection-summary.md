# Face Detection

<div align="center">
<img src='../Images/Face-Detection-Images/detect_faces.png' >
<p>Detected Faces</p>
</div>


Face detection is usually the first step towards many face-related technologies, such as face recognition or verification. However, face detection itself can have very useful applications.
The most successful applications of face detection would probably be photo taking. When you take a photo of your friends, the face detection algorithm built into your digital camera detects where the faces are and adjusts the focus accordingly.

<div align="center">
<img src='../Images/Face-Detection-Images/model.png' >
<p>Face Detection Model</p>
</div>


## Real Time Robust Face and Facial Feature Detection 

This igure illustrates the scheme for face detection based on multiscale search with a classifier of fixed size. First, a pyramid of multiple-resolution versions of the input image is constructed. Then, a face classifier is used to test each subwindow in a subset of these images. At each scale, faces are detected depending on the output of the classifier. The detection results at each scale are projected back to the input image with the appropriate size and position. One chooses the scales to be tested depending on the desired range of size variation allowed in the test image.

<div align="center">
<img src='../Images/Face-Detection-Images/multiscale.jpg' >
<p>General scheme for multiscale face detection</p>
</div>


Each window is preprocessed to deal with illumination variations before it is tested with the classifier. A postprocessing algorithm is also used for face candidate selection. The classification is not carried out independently on each subwindow. Instead, face candidates are selected by analyzing the confidence level of subwindow classifications in neighborhoods so that results from neighboring locations, including those at different scales, are combined to produce a more robust list of face candidates.


<div align="center">
<img src='../Images/Face-Detection-Images/mathematical-equations.png' >
<p>Histogram equalization and requantization</p>
</div>


The face-classification decision is not made independently at every subwindow; instead, face candidates are selected by analyzing these log-likelihood maps locally.

<div align="center">
<img src='../Images/Face-Detection-Images/preprocessing.jpg' >
<p>Examples of the image-preprocessing algorithm</p>
</div>


## Challenges in face detection

Challenges in face detection, are the reasons which reduce the accuracy and detection rateof face detection. These challenges are complex background, too many faces in images, oddexpressions, illuminations, less resolution, face occlusion, skin color, distance and orientation etc.
Some of the challenges are as follows-

1. Odd expressions- Human face in an image may have odd expressions unlike normal, whichis challenge for face detection.
2. Face Occlusion- It is hiding face by any object. It may be glasses, scarf, hand,hairs, hats and any other object etc. It also reduces the face detection rate.
3. Illuminations- Lighting effects may not be uniform in the image. Some part of the imagemay have very high illumination and other may have very low illumination.
4. Less Resolution- Resolution of image may be very poor, which is also challenging for facedetection.
5. Skin color- It changes with geographical locations. Skin color of Chinese is differ-ent from African and skin-color of African is different from American and so on. Changingskin-color is also challenging for face detection.

<div align="center">
<img src='../Images/Face-Detection-Images/challenges.png' >
<p>Various categories of challenges for face detection</p>
</div>

## Applications of Face Detection System

1. Gender Classiﬁcation- Gender information can be found from human being image.
2. Document control and Access control- Control can be imposed to document access withface identiﬁcation system.
3. Human computer interaction system- It is design and use of computer technology, focusing particularly on the interfaces between users and computers.
4. Biometric attendance- It is system of taking attendance of people by their ﬁnger prints orface etc.
5. Photography- Some recent digital cameras use face detection for autofocus. Face detectionis also useful for selecting regions of interest in photo slideshows.
6. Facial feature extraction- Facial features like nose, eyes, mouth, skin-color etc. can beextracted from image.
7. Face recognition- A facial recognition system is a process of identifying or verifying aperson from a digital image or a video frame. One of the ways to do this is by comparingselected facial features from the image and a facial database. It is typically used in security systems.

<div align="center">
<img src='../Images/Face-Detection-Images/face-recognition.jpg' >
<p>Appearance based feature extraction</p>
</div>


## Face detection techniques

Face detection is a computer technology that determines the location and size of a humanface in the digital image. The facial features are detected and any other objects like trees,buildings and bodies are ignored from the digital image. It can be regarded as a speciﬁc caseof object-class detection, where the task is ﬁnding the location and sizes of all objects inan image that belongs to a given class. Face detection, can be seen as a more general caseof face localization. In face localization, the task is to identify the locations and sizes of aknown number of faces (usually one). Basically, there are two types of approaches to detectfacial part in the given digital image i.e. feature based and image based approach.


<div align="center">
<img src='../Images/Face-Detection-Images/techniques.png' >
<p>Face Detection Techniques</p>
</div>
